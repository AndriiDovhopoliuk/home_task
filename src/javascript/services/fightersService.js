import { callApi } from '../helpers/apiHelper';

export async function getFighters() {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id) {
      try {
        let fighter;
        for (let i=0; i < fighters.length; i++) {
          if (fighters[i]._id == id) {
            fighter = fighters[i];
            break;
          }
        }
        if (fighter) {
          if ((!fighter.health)||(!fighters.attack)||(!fighters.defense)) {
            let url = "./resources/api/details/fighter/"+id+".json";
            let response = await fetch(url);
            if (response.ok) { 
              let params = await response.json();
              fighter.health = params.health;
              fighter.attack = params.attack;
              fighter.defense = params.defense;
            } else {
              alert("Error load details ! ("+id+")");
            }
          }
          let curr = this.selectLast ? 1 : 0;
          this.selectLast = !this.selectLast;
          this.selectedFighters[curr] = fighter;
          createFighterPreview(fighter, FighterService.posPreview[curr]);        
          return fighter;
        } else throw "Error";
      } catch (error) {
        alert("Error search fighter "+id+" !");
      };
    }

    async getSelectedFighters() {
      return Promise.resolve(this.selectedFighters);
    }



export const fighterService = new FighterService();
